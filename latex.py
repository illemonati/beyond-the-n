import sympy as sp


def generate_latex_for_data(points, lines):
    res = "Points: \n\n"
    for name, point in points.items():
        res += f"{name} : (${sp.latex(point.x)}$, ${sp.latex(point.y)}$)  \n\n"
    res += "\n"
    res += "Lines: \n\n"
    for name, line in lines.items():
        res += f"{name} : ${line.point_name1} \\leftarrow\\rightarrow {line.point_name2}$  \n\n"
    return res


def matrix_to_latex(matrix):
    res = "\\begin{bmatrix}\n"
    for row in matrix:
        res += "\t"
        for col in row:
            res += f"{sp.latex(col)} & "
        res += "\\\\\n"
    res += "\end{bmatrix}"
    return res


def generate_latex_transformation(transformation, pre_points, post_points):
    res = "Transformation Matrix T: \n\n"
    res += "$$" + matrix_to_latex(transformation) + "$$\n\n"
    return res
