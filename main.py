from utils import parse_input_string, parse_transformation_string, transform
from latex import generate_latex_for_data, generate_latex_transformation
from render import render_data


def load_image(file_path):
    with open(file_path, 'r') as f:
        points, lines = parse_input_string(f.read())
        return points, lines


def load_transformation(file_path):
    with open(file_path, 'r') as f:
        return parse_transformation_string(f.read())


def write_file(file_path, s):
    with open(file_path, 'w+') as f:
        f.write(s)


def main():
    pre_points, pre_lines = load_image("preimage.json")
    print(f"Loaded {len(pre_points)} points")
    print(f"Loaded {len(pre_lines)} lines")
    render_data(pre_points, pre_lines, "preimage", "preimage.png")
    print("Rendered preimage")
    write_file("preimage.tex", generate_latex_for_data(pre_points, pre_lines))
    print("Wrote preimage tex")
    transformation = load_transformation("transformation.json")
    print(f"Loaded Transformation:\n{transformation}")
    image_points = transform(transformation, pre_points)
    print(f"Transformed preimage")
    write_file("transformation.tex", generate_latex_transformation(
        transformation, pre_points, image_points))
    print("Wrote transformation tex")
    render_data(image_points, pre_lines, "image", "image.png")
    print("Rendered image")
    write_file("image.tex", generate_latex_for_data(image_points, pre_lines))
    print("Wrote image tex")


if __name__ == '__main__':
    main()
