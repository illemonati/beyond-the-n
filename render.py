import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
from utils import get_mid_point


def render_data(points, lines, graph_name="graph", save_name="graph.png"):
    xs, ys = {}, {}
    fig, ax = plt.subplots()
    for name, point in points.items():
        xs[name] = point.x
        ys[name] = point.y
        ax.plot(point.x, point.y, 'go')
        ax.annotate(name, xy=(point.x, point.y),
                    xytext=(point.x+0.2, point.y+0.2))

    for name, line in lines.items():
        x1 = xs[line.point_name1]
        x2 = xs[line.point_name2]
        y1 = ys[line.point_name1]
        y2 = ys[line.point_name2]
        ax.plot([x1, x2], [y1, y2], 'k-')
        xm, ym = get_mid_point(x1, x2, y1, y2)
        ax.annotate(name, xy=(xm, ym), xytext=(xm-0.3, ym-0.4))

    loc = plticker.MultipleLocator(base=1)
    ax.xaxis.set_major_locator(loc)
    ax.yaxis.set_major_locator(loc)

    ax.set_title(graph_name)
    ax.grid("on")
    fig.savefig(save_name)
