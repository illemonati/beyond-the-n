import json
import sympy as sp
import numpy as np


class Point:
    def __init__(self, x, y, name=None):
        self.x = x
        self.y = y
        if name is not None:
            self.name = name
        else:
            self.name = "unamed"

    def as_np(self):
        return np.array([self.x, self.y])

    def __repr__(self):
        return f"Point<{self.name}>({self.x}, {self.y})"


class Line:
    def __init__(self, point_name1, point_name2, name=None):
        self.point_name1 = point_name1
        self.point_name2 = point_name2
        if name is not None:
            self.name = name
        else:
            self.name = "unamed"

    def __repr__(self):
        return f"Line<{self.name}>({self.point_name1}, {self.point_name2})"


def parse_input_string(input_string):
    obj = json.loads(input_string)
    points_raw = obj["points"]
    points = {}
    for name, (x, y) in points_raw.items():
        points[name] = Point(sp.Rational(x), sp.Rational(y), name)
    lines_raw = obj["lines"]
    lines = {}
    for name, (a, b) in lines_raw.items():
        lines[name] = Line(a, b, name)
    return points, lines


def get_mid_point(x1, x2, y1, y2):
    return (x1+x2)/2, (y1+y2)/2


def parse_transformation_string(transformation_string):
    obj = json.loads(transformation_string)
    for row in obj:
        for i, x in enumerate(row):
            row[i] = sp.Rational(x)

    return np.array(obj)


def transform(transformation, preimage_points):
    image_points = {}
    for name, point in preimage_points.items():
        res_point = np.dot(transformation, point.as_np())
        image_points[name] = Point(res_point[0], res_point[1], name)
    return image_points
